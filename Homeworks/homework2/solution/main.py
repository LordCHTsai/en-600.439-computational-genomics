'''
File: main.py
Homework 2 - Problem 6

Created on 2013/9/28
@author: Cheng-Hsuan Tsai
'''
import sys
import argparse
from collections import defaultdict

def phred33ToQ(qual):
  return ord(qual) - 33

def stdin_fastq_parser():
  data = defaultdict(list)
  key = ""
  lineNum = 0
  try:
    if not sys.stdin.isatty():
      for line in sys.stdin:
        lineNum += 1
        if line:
          if line[0] == "+":
            continue
          else:
            if line[0] == "@" and len(line) < 100:
              key = line.strip()[1:]
              continue
            else:
              if len(key) != 0 and len(line.strip()) != 0:
                data[key].append(line.strip()[:100])
                continue
        else:
          break
    if lineNum == 0:
      sys.stderr.write("[Error] input file is empty.\n")
      sys.exit()
  except:
    sys.stderr.write("[Error] incorrect format.\n")
    sys.exit()
  return data

def summarize(data):
  summary = []
  output = {'A': 0, 'C': 0, 'G': 0, 'T': 0, 'N': 0, 'P<20': 0, 'P>=20': 0}
  for i in range(100):
    summary.append({'A': 0, 'C': 0, 'G': 0, 'T': 0, 'N': 0, 'P<20': 0, 'P>=20': 0})

  for key in data:
    for i in range(100):
      if data[key][0][i] not in output or data[key][0][i] == 'N':
        summary[i]['N'] += 1
      else:
        summary[i][data[key][0][i]] += 1
      Qvalue = phred33ToQ(data[key][1][i])
      if Qvalue < 20:
        summary[i]['P<20'] += 1
      else:
        summary[i]['P>=20'] += 1
  return summary


def main(argv):
  data = stdin_fastq_parser()
  summary = summarize(data)
  for entry in summary:
    print "%d %d %d %d %d %d"%(entry['A'], entry['C'], entry['G'], entry['T'], entry['P<20'], entry['P>=20'])
  pass

if __name__ == '__main__': main(sys.argv)
