'''
File: main.py
Homework 3 - Problem5

Created on 2013/10/9
@author: Cheng-Hsuan Tsai
'''
import sys
import argparse
from collections import defaultdict

def parse_arguments():
  parser = argparse.ArgumentParser()
  parser.add_argument("-t", help="text file", required=True)
  parser.add_argument("-p", help="pattern file", required=True)
  parser.add_argument("-e", type=int, help="max edit distance", required=True)
  parser.add_argument("-c", type=int, help="ignore letter case", required=False)
  args = parser.parse_args()
  return args

def patternPreprocessed(patternFile, maxEditDist):
  patterns = defaultdict(list)
  indexLen = 0
  try:
    tempFile = open(patternFile)
    lines = tempFile.readlines()
    lineSize = len(lines[0])
    for line in lines:
      if line and len(line) == lineSize:
        line = line.replace('\r\n','').replace('\n','')
        indexLen = len(line)-maxEditDist
        for i in range(0,maxEditDist+1):
          patterns[line].append(line[i:i+indexLen:2])
      else:
        tempFile.close()
        break
  except:
    sys.stderr.write("[Error] incorrect format or file not exist.\n")
    sys.exit()
  return patterns, indexLen

def textParsingAndIndexing(textFile, indexLen):
  indexDict = defaultdict(list)
  try:
    tempFile = open(textFile)
    data = tempFile.read().replace('\r\n','').replace('\n','')
    tempFile.close()
    indexDict = indexing(data, indexLen)
  except:
    sys.stderr.write("[Error] incorrect format or file not exist.\n")
    sys.exit()
  return data, indexDict

def indexing(text, indexLen):
  indexDict = defaultdict(list)
  for i in range(len(text)-indexLen+1):
    indexDict[text[i:i+indexLen:2]].append(i)
  return indexDict

def matching(patterns, data, indexDict, maxEditDist, letterCase):
  result = defaultdict(lambda: defaultdict(list))
  for key in patterns:
    offset = 0
    for partition in patterns[key]:
      offset = int(patterns[key].index(partition))
      for hit in indexDict[partition]:
        mismatch = 0
        result[key]['indexHits'].append(hit)
        position = hit
        if position - offset < 0:
          continue
        if position + len(key) - offset > len(data):
          continue
        for i in range(0, len(key)):
          if letterCase:
            if data[position-offset+i].lower() != key[i].lower():
              mismatch += 1
              if mismatch > maxEditDist:
                break
          else:
            if data[position-offset+i] != key[i]:
              mismatch += 1
              if mismatch > maxEditDist:
                break
        if mismatch <= maxEditDist:
          if position-offset not in result[key][mismatch]:
            result[key][mismatch].append(position-offset)
  return result

def main(argv):
  args = parse_arguments()
  patterns, indexLen = patternPreprocessed(args.p, args.e)
  data , indexDict= textParsingAndIndexing(args.t, indexLen)
  result = matching(patterns, data, indexDict, args.e, args.c)
  for key in result:
    sys.stdout.write(key+": ")
    distinctMatch = 0
    for i in range(args.e+1):
      distinctMatch += len(result[key][i])
      sys.stdout.write(str(len(result[key][i]))+" ")
    print "%.4f"%(float(distinctMatch)/len(result[key]['indexHits']))
  pass

if __name__ == '__main__': main(sys.argv)
