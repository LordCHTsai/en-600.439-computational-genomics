'''
File: main.py
Homework 3 - Problem 1

Created on 2013/10/8
@author: Cheng-Hsuan Tsai
'''
import sys
import argparse
import RNACodonTable

def stdin_parser():
  data = {}
  key = ""
  lineNum = 0
  try:
    if not sys.stdin.isatty():
      for line in sys.stdin:
        lineNum += 1
        if line:
          if line[0] == ">" or line[0] == ";":
            key = line.strip()
            data[key] = ""
          else:
            if len(key) != 0 and len(line.strip()) != 0:
              data[key] += line.strip()
        else:
          break
    if lineNum == 0:
      sys.stderr.write("[Error] input file is empty.\n")
      sys.exit()
  except:
    sys.stderr.write("[Error] incorrect format.\n")
    sys.exit()
  return data


def ProteinTranslation(data):
  for key in data:
    startPos = data[key].find("AUG")
    if startPos != -1:
      for i in range(startPos, len(data[key]), 3):
        if RNACodonTable.rnaMap[str(data[key][i:i+3])] != "STOP":
          sys.stdout.write(RNACodonTable.rnaMap[str(data[key][i:i+3])])
        else:
          break
    sys.stdout.write("\n")
  pass

def intronExonHandling(data):
  result = {}
  longestStr = 0
  longestKey = ""

  for key in data:
    if len(data[key]) > longestStr:
      longestStr = len(data[key])
      longestKey = key
  result[longestKey] = data[longestKey]

  for key in data:
    if key != longestKey:
      result[longestKey] = result[longestKey].replace(data[key], "")
  return result

def Transcription(data):
  for key in data:
    data[key] = data[key].replace("T","U")
  return data

def main(argv):
  data = stdin_parser()
  catResult = intronExonHandling(data)
  RNA = Transcription(catResult)
  ProteinTranslation(RNA)
  pass

if __name__ == '__main__': main(sys.argv)
