'''
File: main.py
Homework 3 - Problem2

Created on 2013/10/9
@author: Cheng-Hsuan Tsai
'''
import sys
import argparse
from itertools import permutations
from collections import defaultdict

def stdin_parser():
  data = {}
  key = ""
  undefined = "Undefined_Key_"
  lineNum = 0
  keyNum = 0
  try:
    if not sys.stdin.isatty():
      for line in sys.stdin:
        lineNum += 1
        if line:
          if line[0] == ">" or line[0] == ";":
            key = line.strip()
            data[key] = ""
            keyNum += 1
          else:
            if len(key) != 0 and len(line.strip()) != 0:
              data[key] += line.strip()
            else:
              data[undefined + str(keyNum)] = line.strip()
              keyNum += 1
        else:
          break
    if lineNum == 0:
      sys.stderr.write("[Error] input file is empty.\n")
      sys.exit()
  except:
    sys.stderr.write("[Error] incorrect format.\n")
    sys.exit()
  return data

def main(argv):
  data = stdin_parser()
  for key in data:
    print len(list(permutations(range(1,int(data[key])+1))))
    for row in permutations(range(1,int(data[key])+1)):
      for element in row:
        sys.stdout.write(str(element)+" ")
      print
  pass

if __name__ == '__main__': main(sys.argv)
