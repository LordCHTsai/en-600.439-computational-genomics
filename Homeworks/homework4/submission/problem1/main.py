'''
File: main.py
Homework 4 - Problem 1

Created on 2013/11/1
@author: Cheng-Hsuan Tsai
'''
import sys
import argparse

def stdin_parser():
  data = {}
  key = ""
  lineNum = 0
  try:
    if not sys.stdin.isatty():
      for line in sys.stdin:
        lineNum += 1
        if line:
          if line[0] == ">" or line[0] == ";":
            key = line.strip()
            data[key] = ""
          else:
            if len(key) != 0 and len(line.strip()) != 0:
              data[key] += line.strip()
        else:
          break
    if lineNum == 0:
      sys.stderr.write("[Error] input file is empty.\n")
      sys.exit()
  except:
    sys.stderr.write("[Error] incorrect format.\n")
    sys.exit()
  return data

def rotations(t):
  tt = t*2
  return [ tt[i:i+len(t)] for i in xrange(0, len(t))]

def bwm(t):
  return sorted(rotations(t))

def bwtViaBwm(t):
  return ''.join(map(lambda x: x[-1], bwm(t)))

def rankBwt(bw):
  tots = dict()
  ranks = []
  for c in bw:
    if c not in tots: tots[c] = 0
    ranks.append(tots[c])
    tots[c] += 1
  return ranks, tots

def firstCol(tots):
  first = {}
  totc = 0
  for c, count in sorted(tots.iteritems()):
      first[c] = (totc, totc + count)
      totc += count
  return first

def reverseBwt(bw):
  ranks, tots = rankBwt(bw)
  first = firstCol(tots)
  rowi = 0
  t = '$'
  while bw[rowi] != '$':
    c = bw[rowi]
    t = c + t
    rowi = first[c][0] + ranks[rowi]
  return t

def commonSubstrings(bw, string):
  substrings = []
  ranks, tots = rankBwt(bw)
  first = firstCol(tots)
  for i in xrange(len(string)):
    temp = ''
    c = string[len(string)-i-1]
    if c in first:
      L = first[c][0]
      mapRange = first[c][1] - L
    else:
      L = 0
      mapRange = 0
    for j in xrange(len(string)-i-1, -1, -1):
      c = string[j]
      if mapRange > 0:
        temp = c + temp
        if temp not in substrings and len(temp) > 0:
          substrings.append(temp)
      else:
        break
      if j != 0:
        c = string[j-1]
        pos = bw.find(c, L, L+mapRange)
        mapRange = bw[L:L+mapRange].count(c)
        if pos >= 0:
          rank = ranks[pos]
          L = first[c][0] + rank
        else:
          L = 0
          mapRange = 0
  return substrings

def longestCommonSubstring(data):
  existBWT = False
  existSubstrings = False
  bw = ''
  substrings = []
  for key in data:
    if not existBWT:
      bw = bwtViaBwm(data[key] + '$')
      existBWT = True
      continue
    if not existSubstrings:
      substrings = commonSubstrings(bw, data[key])
      existSubstrings = True
      continue
    for substring in substrings:
      if data[key].find(substring) < 0:
        substrings.remove(substring)
  longest = ''
  for substring in substrings:
    if len(substring) > len(longest):
      longest = substring
  return longest

def main(argv):
  data = stdin_parser()
  print longestCommonSubstring(data)
  pass

if __name__ == '__main__': main(sys.argv)
