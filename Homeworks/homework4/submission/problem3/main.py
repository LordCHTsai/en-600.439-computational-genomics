'''
File: main.py
Homework 4 - Problem 3

Created on 2013/11/6
@author: Cheng-Hsuan Tsai
'''
import sys
import argparse
import numpy

def stdin_parser():
  data = {}
  key = ""
  lineNum = 0
  try:
    if not sys.stdin.isatty():
      for line in sys.stdin:
        lineNum += 1
        if line:
          if line[0] == ">" or line[0] == ";":
            key = line.strip()
            data[key] = ""
          else:
            if len(key) != 0 and len(line.strip()) != 0:
              data[key] += line.strip()
        else:
          break
    if lineNum == 0:
      sys.stderr.write("[Error] input file is empty.\n")
      sys.exit()
  except:
    sys.stderr.write("[Error] incorrect format.\n")
    sys.exit()
  return data

def edDistDp(data):
  t = ''
  p = ''
  for key in data:
    if len(t) == 0:
      t = data[key]
      continue
    if len(p) == 0:
      p = data[key]
      continue
  if len(p) > len(t):
    p, t = t, p
  D = numpy.zeros((len(t)+1, len(p)+1), dtype=int)
  D[0, 1:] = range(1, len(p)+1)
  D[1:, 0] = range(1, len(t)+1)
  for i in xrange(1, len(t)+1):
    for j in xrange(1, len(p)+1):
      delt = 1 if t[i-1] != p[j-1] else 0
      D[i, j] = min(D[i-1, j-1]+delt, D[i-1, j]+1, D[i, j-1]+1)
  return D[len(t), len(p)]

def main(argv):
  data = stdin_parser()
  edit = edDistDp(data)
  print edit
  pass

if __name__ == '__main__': main(sys.argv)
