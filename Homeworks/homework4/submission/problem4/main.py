'''
File: main.py
Homework 4 - Problem 4

Created on 2013/11/7
@author: Cheng-Hsuan Tsai
'''
import sys
import argparse
import numpy

def stdin_parser():
  data = {}
  key = ""
  lineNum = 0
  sequence = []
  try:
    if not sys.stdin.isatty():
      for line in sys.stdin:
        lineNum += 1
        if line:
          if line[0] == ">" or line[0] == ";":
            key = line.strip()
            sequence.append(line.strip())
            data[key] = ""
          else:
            if len(key) != 0 and len(line.strip()) != 0:
              data[key] += line.strip()
        else:
          break
    if lineNum == 0:
      sys.stderr.write("[Error] input file is empty.\n")
      sys.exit()
  except:
    sys.stderr.write("[Error] incorrect format.\n")
    sys.exit()
  return data, sequence

def edDistDp(data, seq):
  t = ''
  p = ''
  for i in range(len(seq)):
    if len(t) == 0:
      t = data[seq[i]]
      continue
    if len(p) == 0:
      p = data[seq[i]]
      continue
  D = numpy.zeros((len(t)+1, len(p)+1), dtype=int)
  D[0, 1:] = range(1, len(p)+1)
  D[1:, 0] = range(1, len(t)+1)
  for i in xrange(1, len(t)+1):
    for j in xrange(1, len(p)+1):
      delt = 1 if t[i-1] != p[j-1] else 0
      D[i, j] = min(D[i-1, j-1]+delt, D[i-1, j]+1, D[i, j-1]+1)
  align = backTrace(D, t, p)
  return D[len(t), len(p)], align

def backTrace(D, t, p):
  alignT = ''
  alignP = ''
  i, j = len(t), len(p)
  while i > 0 or j > 0:
    if i > 0 and j > 0:
      m = min(D[i-1, j-1], D[i-1, j], D[i, j-1])
      if m == D[i-1, j-1] and (D[i, j] == m + 1 or D[i, j] == m):
        alignT = t[i-1] + alignT
        alignP = p[j-1] + alignP
        i -= 1
        j -= 1
        continue
      else:
        if m == D[i-1, j] and D[i, j] == m + 1:
          alignT = t[i-1] + alignT
          alignP = '-' + alignP
          i -= 1
          continue
        if m == D[i, j-1] and D[i, j] == m + 1:
          alignT = '-' + alignT
          alignP = p[j-1] + alignP
          j -= 1
          continue
    else:
      if i == 0:
        alignT = t[j-1] + alignT
        alignP = '-' + alignP
        j -= 1
        continue
      if j == 0:
        alignT = '-' + alignT
        alignP = p[i-1] + alignP
        i -= 1
        continue
  return [alignT, alignP]

def main(argv):
  data, seq = stdin_parser()
  edit, align = edDistDp(data, seq)
  print edit
  print align[0]
  print align[1]
  pass

if __name__ == '__main__': main(sys.argv)
