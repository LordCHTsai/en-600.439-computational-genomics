'''
File: main.py
Homework 4 - Problem 2

Created on 2013/11/6
@author: Cheng-Hsuan Tsai
'''
import sys
import argparse

def stdin_parser():
  data = {}
  key = ""
  lineNum = 0
  try:
    if not sys.stdin.isatty():
      for line in sys.stdin:
        lineNum += 1
        if line:
          if line[0] == ">" or line[0] == ";":
            key = line.strip()
            data[key] = ""
          else:
            if len(key) != 0 and len(line.strip()) != 0:
              data[key] += line.strip()
        else:
          break
    if lineNum == 0:
      sys.stderr.write("[Error] input file is empty.\n")
      sys.exit()
  except:
    sys.stderr.write("[Error] incorrect format.\n")
    sys.exit()
  return data

def subSequenceSimpleVer(data):
  t = ''
  p = ''
  for key in data:
    if len(t) == 0:
      t = data[key]
      continue
    if len(p) == 0:
      p = data[key]
      continue
  if len(p) > len(t):
    p, t = t, p
  pos = 0
  for c in p:
    pos = t.find(c, pos) + 1
    sys.stdout.write(str(pos)+' ')

def main(argv):
  data = stdin_parser()
  subSequenceSimpleVer(data)
  pass

if __name__ == '__main__': main(sys.argv)
