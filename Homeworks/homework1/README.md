#Homework1
@author: Cheng-Hsuan Tsai

The solution programs are put in each directory which is corresponding to each problem in homework1.
All programs are coded in Python 2.7 w/o any external dependecies and are tested on both JHU-CS undergraduate and graduate cluster, each directory also includes a sample dataset file and a sample output file.

##1.Counting DNA Nucleotides
Usage: `python main.py < input > output`
Example: `python main.py < rosalind_dna.txt > result.txt`

##2.Transcribing DNA into RNA
Usage: `python main.py < input > output`
Example: `python main.py < rosalind_rna.txt > result.txt`

##3.Complementing a Strand of DNA
Usage: `python main.py < input > output`
Example: `python main.py < rosalind_revc.txt > result.txt`

##4.Counting Point Mutations
Usage: `python main.py < input > output`
Example: `python main.py < rosalind_hamm.txt > result.txt`

##5.Protein Translation
Usage: `python main.py < input > output`
Example: `python main.py < rosalind_prot.txt > result.txt`

##6.Finding a Motif in DNA
Usage: `python main.py < input > output`
Example: `python main.py < rosalind_subs.txt > result.txt`

##7.Computing GC Content
Usage: `python main.py < input > output`
Example: `python main.py < rosalind_gc.txt > result.txt`

