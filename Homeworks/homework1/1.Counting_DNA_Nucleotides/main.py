'''
File: main.py
Homework 1 - 1.Counting DNA Nucleotides

Created on 2013/9/15
@author: Cheng-Hsuan Tsai
'''
import sys
import argparse

def stdin_parser():
  data = {}
  key = ""
  undefined = "Undefined_Key_"
  lineNum = 0
  keyNum = 0
  try:
    if not sys.stdin.isatty():
      for line in sys.stdin:
        lineNum += 1
        if line:
          if line[0] == ">" or line[0] == ";":
            key = line.strip()
            data[key] = ""
            keyNum += 1
          else:
            if len(key) != 0 and len(line.strip()) != 0:
              data[key] += line.strip()
            else:
              data[undefined + str(keyNum)] = line.strip()
              keyNum += 1
        else:
          break
    if lineNum == 0:
      sys.stderr.write("[Error] input file is empty.\n")
      sys.exit()
  except:
    sys.stderr.write("[Error] incorrect format.\n")
    sys.exit()
  return data


def CountNucleotides(data, sequenceToPrint):
  for key in data:
    for nucleotide in sequenceToPrint:
      sys.stdout.write(str(data[key].count(nucleotide)) + " ")
    sys.stdout.write("\n")
  pass

def main(argv):
  data = stdin_parser()
  CountNucleotides(data, ['A','C','G','T'])
  pass

if __name__ == '__main__': main(sys.argv)
