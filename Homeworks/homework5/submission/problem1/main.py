'''
File: main.py
Homework 5 - Problem 1

Created on 2013/11/20
@author: Cheng-Hsuan Tsai
'''
import sys
import argparse
import RNACodonTable

def stdin_parser():
  data = {}
  key = ""
  lineNum = 0
  try:
    if not sys.stdin.isatty():
      for line in sys.stdin:
        if line:
          lineNum += 1
          if line[0] == ">" or line[0] == ";":
            key = line.strip()
            data[key] = ""
          else:
            if len(key) != 0 and len(line.strip()) != 0:
              data[key] += line.strip()
        else:
          break
    if lineNum == 0:
      sys.stderr.write("[Error] input file is empty.\n")
      sys.exit()
  except:
    sys.stderr.write("[Error] incorrect format.\n")
    sys.exit()
  return data

def ProteinTranslation(data):
  returnList = []
  for key in data:
    for i in range(3):
      temp = ""
      for j in range(i, len(data[key]), 3):
        if j+3 < len(data[key]):
          temp += RNACodonTable.rnaMap[str(data[key][j:j+3])]
      index = 0
      while( temp.find('M',index) != -1 and temp.find('.',temp.find('M',index)) != -1):
        returnList.append(temp[ temp.find('M',index) : temp.find('.', temp.find('M',index))])
        index = temp.find('M', index) + 1
  return returnList

def reverse(data):
  returnData = {}
  for key in data:
    returnData[key] = data[key][::-1];
  return returnData

def complement(data):
  returnData = {}
  complementDict = {'A':'T', 'T':'A', 'C':'G', 'G':'C'}
  for key in data:
    temp = ""
    for i in range(len(data[key])):
      temp += complementDict[data[key][i]]
    returnData[key] = temp
  return returnData

def DNAtoRNA(data):
  returnData = {}
  for key in data:
    returnData[key] = data[key].replace("T","U")
  return returnData

def main(argv):
  data = stdin_parser()
  data1 = reverse(data)
  data1 = complement(data1)
  data1 = DNAtoRNA(data1)
  data2 = DNAtoRNA(data)
  ORFlist = list( set(ProteinTranslation(data1)) | set(ProteinTranslation(data2)) )
  for i in range(len(ORFlist)):
    print ORFlist[i]
  pass

if __name__ == '__main__': main(sys.argv)
